export function isAppleDevice(): boolean {
	return /(Mac|iPhone|iPod|iPad)/i.test(typeof window === 'undefined' ? '' : navigator?.platform);
}

type CommandKeys = '⌘' | 'Ctrl';
export function getPlatformCommandKey(): CommandKeys {
	return isAppleDevice() ? '⌘' : 'Ctrl';
}

type AltKeys = '⌥' | 'Alt';
export function getPlatformAlternateKey(): AltKeys {
	return isAppleDevice() ? '⌥' : 'Alt';
}

type ShiftKeys = 'Shift' | '↑';
export function getPlatformShiftKey(): ShiftKeys {
	return isAppleDevice() ? 'Shift' : '↑';
}
